﻿#include <iostream>
#include <string>
using namespace std;

class Animal
{
public:
    virtual void Voice()
    {
        cout << "Animal voice";
    }
    
    virtual ~Animal()
    {}

};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof!" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow!" << endl;
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "Mooo!" << endl;
    }
};

class Sheep : public Animal
{
public:
    void Voice() override
    {
        cout << "Baaa!" << endl;
    }
};

class Pig : public Animal
{
public:
    void Voice() override
    {
        cout << "Oink!" << endl;
    }
};

int main()
{
    const int SIZE = 5;

   Animal* arr[SIZE] = { new Dog, new Cat, new Cow,new Sheep,new Pig };

    for (int i = 0; i < SIZE; i++)
    {
        arr[i]->Voice();
        delete arr[i];
    }

   
    return 0;
}  

